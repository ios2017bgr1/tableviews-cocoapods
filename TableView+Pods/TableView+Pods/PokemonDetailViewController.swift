//
//  PokemonDetailViewController.swift
//  TableView+Pods
//
//  Created by Sebastian Guerrero F on 1/8/18.
//  Copyright © 2018 SG. All rights reserved.
//

import UIKit

class PokemonDetailViewController: UIViewController {
  
  var pokemon:Pokemon?
  
  @IBOutlet weak var pokemonImgeView: UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = pokemon?.pkName
    
    let pkService = PokemonService()
    pkService.getPokemonImage(id: (pokemon?.pkId)!) { (pkImage) in
      self.pokemonImgeView.image = pkImage
    }
    
  }
}
