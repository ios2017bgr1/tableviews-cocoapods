//
//  PokemonTableViewCell.swift
//  TableView+Pods
//
//  Created by Sebastian Guerrero on 1/5/18.
//  Copyright © 2018 SG. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {
  
  @IBOutlet weak var idLabel: UILabel!
  @IBOutlet weak var nameLabel: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  func fillData(_ pokemon:Pokemon){
    idLabel.text = "\(pokemon.pkId ?? 0)"
    nameLabel.text = pokemon.pkName
  }
  
}
