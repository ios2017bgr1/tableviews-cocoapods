//
//  Pokemon.swift
//  TableView+Pods
//
//  Created by Sebastian Guerrero on 11/27/17.
//  Copyright © 2017 SG. All rights reserved.
//

import Foundation
import ObjectMapper

class Pokemon:Mappable {
  
  var pkId:Int?
  var pkName:String?
  var pkWeight:Double?
  var pkHeight:Double?
  
  required init?(map: Map){
  }
  
  func mapping(map: Map) {
    pkId <- map["id"]
    pkName <- map["name"]
    pkWeight <- map["weight"]
    pkHeight <- map["height"]
  }
}
