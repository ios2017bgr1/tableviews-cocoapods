//
//  PokemonService.swift
//  TableView+Pods
//
//  Created by Sebastian Guerrero on 1/5/18.
//  Copyright © 2018 SG. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import AlamofireImage

protocol PokemonServiceDelegate {
  func first20Pokemon(_ pokemon:[Pokemon])
}

class PokemonService {
  
  var delegate:PokemonServiceDelegate?
  
  func get20FirstPokemon() {
    
    var pokemonArray:[Pokemon] = []
    let group = DispatchGroup()
    
    for i in 1...3 {
      
      group.enter()
      Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseObject { (response: DataResponse<Pokemon>) in
        
        let pokemon = response.result.value
        pokemonArray.append(pokemon!)
        group.leave()
      }
    }
    
    group.notify(queue: .main) {

      self.delegate?.first20Pokemon(pokemonArray.sorted(by: {$0.pkId! < $1.pkId!}))
    }
  }
  
  func getPokemonImage(id:Int , completion:@escaping (UIImage)->()) {
    
    let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
    Alamofire.request(url).responseImage { response in
      debugPrint(response)
      
      if let image = response.result.value {
        completion(image)
      }
    }

  }
  
}





