//
//  ViewController.swift
//  TableView+Pods
//
//  Created by Sebastian Guerrero on 11/27/17.
//  Copyright © 2017 SG. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {
  
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var weightLabel: UILabel!
  @IBOutlet weak var heightLabel: UILabel!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  @IBAction func consultarButtonPressed(_ sender: Any) {
   
    Alamofire.request("https://pokeapi.co/api/v2/pokemon/1/").responseObject { (response: DataResponse<Pokemon>) in
      
      let pokemon = response.result.value
      
      DispatchQueue.main.async {
        self.nameLabel.text = pokemon?.pkName
        self.weightLabel.text = "\(pokemon?.pkWeight ?? 0)"
        self.heightLabel.text = "\(pokemon?.pkHeight ?? 0)"
      }
      
      
    }
    
  }
}

