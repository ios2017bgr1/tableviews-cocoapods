//
//  PokedexViewController.swift
//  TableView+Pods
//
//  Created by Sebastian Guerrero on 1/5/18.
//  Copyright © 2018 SG. All rights reserved.
//

import UIKit

class PokedexViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate {
  
  //MARK:- Outlets
  @IBOutlet weak var pokedexTableView: UITableView!
  
  //MARK:- Attributes
  let pokemonService = PokemonService()
  var pokemonArray:[Pokemon] = []
  var selectedPokemon = 0
  
  //MARK:- ViewController lifecycle
  override func viewDidLoad() {
    super.viewDidLoad()
    pokemonService.delegate = self
  }
  
  override func viewWillAppear(_ animated: Bool) {
    pokemonService.get20FirstPokemon()
  }
  
  //MARK:- PokemonServiceDelegate
  
  func first20Pokemon(_ pokemon: [Pokemon]) {
    pokemonArray = pokemon
    pokedexTableView.reloadData()
  }
  
  
  // MARK:- TableView
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
    case 0:
      return pokemonArray.count
    default:
      return 5
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
    cell.fillData(pokemonArray[indexPath.row])
    return cell
  }
  
  func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
    
    selectedPokemon = indexPath.row
    return indexPath
  }
  
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let destination = segue.destination as! PokemonDetailViewController
    destination.pokemon = pokemonArray[selectedPokemon]
  }
  
  
}
















